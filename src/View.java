import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class View extends JPanel{
    private static final int WINDOW_WIDTH = 390;
    private static final int WINDOW_HEIGHT = 600;

    private static final int MARGIN_X = 20;
    private static final int MARGIN_Y = 60;

    public static JFrame window; // Main window
    public static JTextField displayField; // Input text
    private String[] buttonTexts = new String[]{"C", "<-", "%", "/", "7", "8", "9", "*", "4", "5", "6", "-", "1",
    "2", "3", "+", ".", "0", "="};
    public static MathOperation mathOperation = new MathOperation();
    public static Boolean isCleanDisplay=false;
    private ArrayList<Button> buttons = new ArrayList<>();

    View(){
        window = new JFrame("Calculator");
        window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        window.setLocationRelativeTo(null); // Move Window To Center

        int j = 0;
        int k = 1;
        int[] x = {MARGIN_X, MARGIN_X + 90, MARGIN_X + 180, MARGIN_X + 270, MARGIN_X + 360};
        int[] y = {MARGIN_Y, MARGIN_Y + 100, MARGIN_Y + 180, MARGIN_Y + 260, MARGIN_Y + 340, MARGIN_Y + 420};

        displayField = new JTextField("0");
        displayField.setBounds(x[0], y[0], 350, 70);
        displayField.setEditable(false);
        displayField.setBackground(Color.WHITE);
        window.add(displayField);

for (String buttonText : buttonTexts){
    Button button = new Button(x[j], y[k], buttonText);

    j++;
    if (j == 4){
        k++;
        j=0;
    }

    buttons.add(button);
}



        window.setLayout(null);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setVisible(true);

    }

}
