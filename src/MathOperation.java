public class MathOperation {
    public double num1;
    public double num2;
    public String action;
    public Boolean isRepeat=false;

    public static double calc(double x, double y, String opt) {

        switch (opt) {
            case "+":
                return x + y;
            case "-":
                return x - y;
            case "*":
                return x * y;
            case "/":
                return x / y;
            default:
        }

        return y;
    }

}
