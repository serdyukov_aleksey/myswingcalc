import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Button extends JButton{

    private static final int BUTTON_WIDTH = 80; // Button width
    private static final int BUTTON_HEIGHT = 70; // Button height

    private JTextField displayField = View.displayField;
    private Boolean isCleanDisplay = View.isCleanDisplay;


    private class ActionOperation implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            View.mathOperation.num1 = Double.parseDouble(displayField.getText());
            View.mathOperation.action = actionEvent.getActionCommand();
            View.mathOperation.isRepeat = false;
            View.isCleanDisplay = true;
        }
    }

    Button(int posX, int posY, String text) {
        JButton btn = new JButton(text);
        btn.setBounds(posX, posY, BUTTON_WIDTH, BUTTON_HEIGHT);
        ActionOperation ao = new ActionOperation();
        switch (text){
            case "C":
                btn.addActionListener(event -> displayField.setText("0"));
                break;
            case "<-":
                btn.addActionListener(event -> {
                    String str = displayField.getText();
                    StringBuilder str2 = new StringBuilder();
                    for (int i = 0; i < (str.length() - 1); i++) {
                        str2.append(str.charAt(i));
                    }
                    if (str2.toString().equals("")) {
                        displayField.setText("0");
                    } else {
                        displayField.setText(str2.toString());
                    }
                });
                break;
            case "%":
                btn.addActionListener(event -> {
                    View.mathOperation.num2=View.mathOperation.num1*Double.parseDouble(displayField.getText())/100;
                    displayField.setText(String.valueOf(View.mathOperation.num2));
            });
                break;
            case "/":
            case "-":
            case "*":
            case "+":
                btn.addActionListener(ao);
                break;
            case ".":
                btn.addActionListener(event -> {
                    StringBuilder str = new StringBuilder(displayField.getText());
                    if (str.indexOf(".")==-1){
                        str.append(".");
                    }
                    displayField.setText(str.toString());

                });
                break;
            case "=":
                btn.setBounds(posX, posY, BUTTON_WIDTH *2 + 10, BUTTON_HEIGHT);
                btn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        if (!View.mathOperation.isRepeat ){
                            View.mathOperation.num2 = Double.parseDouble(displayField.getText());
                        }
                                Double res = MathOperation.calc(View.mathOperation.num1, View.mathOperation.num2, View.mathOperation.action);

                        displayField.setText("" + res);
                        View.isCleanDisplay = true;
                        View.mathOperation.num1 = Double.parseDouble(String.valueOf(res));
                        View.mathOperation.isRepeat = true;
                        View.displayField.setFont(View.displayField.getFont().deriveFont(Font.BOLD));
                    }
                });
                break;
            default:

                btn.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        View.displayField.setFont(View.displayField.getFont().deriveFont(Font.PLAIN));
                        if (displayField.getText().charAt(0) == '0' && displayField.getText().length()==1) {
                            displayField.setText(actionEvent.getActionCommand());
                            View.isCleanDisplay = false;
                        }else if (View.isCleanDisplay) {
                            displayField.setText(actionEvent.getActionCommand());
                            View.isCleanDisplay = false;
                        } else {
                            displayField.setText(displayField.getText() + actionEvent.getActionCommand());
                        }

                    }
                });

        }


        View.window.add(btn);
    }
}
